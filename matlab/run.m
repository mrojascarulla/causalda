   
 params.sig = -1;
 params.shuff = 20;
 params.bootForce = 1;
 alpha=0.05;

for task = 3:10
    
    p = 0;
    a = 0;
    for i = 1:30

        if exist(sprintf('../mat/%dmatTrainResidualsLasso%d.mat',task,i),'file')
            a = a+1;

            X = load(sprintf('../mat/%dmatTrainResidualsLasso%d.mat', task,i));
            Y = load(sprintf('../mat/%dmatTestResidualsLasso%d.mat',task,i));
            X1 = X.Residuals;
            
            
            %X1 = vertcat(X1(1:10),X1(301:330),X1(500:520))
            X2 = Y.Residuals;
            X2 = X2(1:90)
            
            X = vertcat(X1,X2);
               
            eachRes = size(X1,1)/(task)%/(task+1);

            thr = zeros(task+1,1);
            lev = zeros(task+1,1);
            
            for j = 1:(task+1)
                mask = ones(size(X));
                mask(1+(j-1)*eachRes:(j)*eachRes) = 0;
                X1 = X(1+(j-1)*eachRes:(j)*eachRes);
                
                X2 = X(logical(mask));
                ix = randperm(size(X2,1));
           
                X2 = X2(ix);
                X2 = X2(1:eachRes);
                 
                [testStat,thresh,params,pval] = mmdTestGamma(X1,X2,alpha/(task+1),params);
                thr(j) = thresh;
                lev(j) = testStat;
                
                %if testStat<=thresh
                %p = p+1;
                %end
                %a = a+1
            end
            
            thr;
            lev;
            
            minpval = min(lev);
            if all(lev<=thr)
                p = p+1;
            end
           
        end
    end
    a
    p
    p./a
    pause
    
end