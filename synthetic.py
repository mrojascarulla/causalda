# Copyright (c) 2014 - 2015  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

from lossFunction import *
from utils import *
from sklearn import datasets, linear_model
from scipy import optimize
from sklearn.kernel_approximation import RBFSampler
from matplotlib import pyplot as pl
from matplotlib import rc
import matplotlib as mpl
from sklearn.metrics import r2_score
import scipy
import pickle

mpl.rc("figure",facecolor="white")
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

numSamples = 90
numSamplesTest = 3
numPredictors = 10
numCauses = 4
numEffects = numPredictors-numCauses
testMult = 5000/numSamples

np.random.seed(42)

#--------------------------------------------

# Results in a file 'errors' with the explained variance for the different
# methods considered as a function of the number of seen labeled examples in
# the test task.

# isPoly : 1 if polynomial kernel, 0 if RBF
# iterations: maximum number of iterations for the gradient descent algorithm
# ls: if using an RBF kernel, length scale of the kernel
# kerPow: if using a polynomial kernel, degree of the polynomial
# computeMMD: if set to True will save the matrices of residuals in the mat/ folder
# numIters : number of repetitions
# samplesTest: number of seen labeled examples for the test task
# tasksArray: number of training tasks
# paramList: regularisation parameters for cross-validation
# errors_file: name of the file in which the expalined variances are to be saved
# printMoments: set to True if the code has already been run and errors have been saved
# in errors_file. Computes mean and std for each approach as the number of labeled
# examples varies.

#---------------------------------------------


isPoly = 1
iterations = 1000
ls = 0.01
kerPow = 2
computeMMD = False
printMoments = True

numIters = 30

samplesTest = np.array([0,2,3,4,6,7,8,9,10])
#samplesTest = np.array([5])
sampMax = max(samplesTest)

noiseY = 0.5

tasksArray = np.arange(3,11)
tasksArray = np.array([7])

maxTask = np.max(tasksArray)

paramList = np.array([1e-10,1e-8,1e-4,1e-2,0.1,1,10,100])
errors_file = 'errors'

errorNumSamples = np.zeros((numIters,samplesTest.size,6,tasksArray.size))

if printMoments:

	with open(errors_file,'rb') as f:
		errorNumSamples = pickle.load(f)

	print "Mean and std for betaPoolR"
	print np.mean(errorNumSamples[:,:,0,-1],axis = 0)
	print np.std(errorNumSamples[:,:,0,-1],axis = 0)

	print "Mean and std for betaPoolL"
	print np.mean(errorNumSamples[:,:,5,-1],axis = 0)
	print np.std(errorNumSamples[:,:,5,-1],axis = 0)

	print "Mean and std for betaInv"
	print np.mean(errorNumSamples[:,:,1,-1],axis = 0)
	print np.std(errorNumSamples[:,:,1,-1],axis = 0)

	print "Mean and std for betaCau"
	print np.mean(errorNumSamples[:,:,4,-1],axis = 0)
	print np.std(errorNumSamples[:,:,4,-1],axis = 0)

	print "Mean and std for betaInv+"
	print np.mean(errorNumSamples[:,:,2,-1],axis = 0)
	print np.std(errorNumSamples[:,:,2,-1],axis = 0)

	print "Mean and std for betaDom"
	print np.mean(errorNumSamples[:,:,3,-1],axis = 0)
	print np.std(errorNumSamples[:,:,3,-1],axis = 0)

	sys.exit()


def generateTask(numCauses, numEffects, alphaC):

	Sx = np.random.uniform(-2,2,(numCauses,1))
	SigmaX = np.dot(Sx,Sx.T)

	r = np.eye(numCauses)
	re = np.eye(numEffects)
	Se = np.random.uniform(-1,1,(numEffects,1))
	Sigmae = np.dot(Se,Se.T)

	Sxe = np.random.uniform(-1,1,(numEffects,1))
	SxeC = np.random.uniform(-1,1,(numCauses,1))
	SigmaXe = np.dot(Sxe,SxeC.T)

	alphaE = np.random.uniform(-1,1.5,numEffects)

	meanC = np.zeros(numCauses)
	meanC =  np.random.uniform(0,1,numCauses)
	meanE = np.zeros(numEffects)

	Xc = (np.random.multivariate_normal(meanC, SigmaX, numSamples))
	Y = np.sum(Xc*alphaC,1) + np.random.normal(0,noiseY,numSamples)
	Xe =Y[:,np.newaxis]*alphaE + 0.5*(np.random.multivariate_normal(meanE, Sigmae, numSamples)) + 0.05*np.dot(Xc,SigmaXe.T)

	train = [Xc,Y,Xe]

	XcT = (np.random.multivariate_normal(meanC, SigmaX, testMult*numSamples))
	YT = np.sum(XcT*alphaC,1) + (np.random.normal(0,noiseY,testMult*numSamples))
	XeT =YT[:,np.newaxis]*alphaE +  0.5*(np.random.multivariate_normal(meanE, Sigmae, testMult*numSamples)) + 0.05*np.dot(XcT,SigmaXe.T)

	test = [XcT,YT,XeT]

	return train, test


def generateIters(numCauses, numPredictors,numIters):

	XTotI = []
	YTotI = []
	XpI = []
	YpI = []
	XtestIT = []
	YtestIT = []
	XtestI = []
	YtestI = []

	numEffects = numPredictors - numCauses

	for iter in range(numIters):

		alphaC = np.random.uniform(-1,2,numCauses)

		[Xc,Y,Xe], [XcT,YT,XeT] = generateTask(numCauses, numEffects,alphaC)

		X = np.append(Xc,Xe,axis=1)

		XtestT = X[0:sampMax,:]
		YtestT = Y[0:sampMax]
		Xtest = np.append(XcT,XeT, axis = 1)
		Ytest = YT
		XtestIT.append(XtestT)
		YtestIT.append(YtestT)
		XtestI.append(Xtest)
		YtestI.append(Ytest)

		XTotT = []
		YTotT = []
		XpT = np.empty((1,numPredictors))
		YpT = np.empty(1)

		for t in range(maxTask):

			[Xc,Y,Xe], [XcT,YT,XeT] = generateTask(numCauses, numEffects,alphaC)
			X = np.append(Xc,Xe,axis=1)

			XTotT.append(X)
			YTotT.append(Y)
			XpT = np.append(XpT, X,axis = 0)
			YpT = np.append(YpT,Y)

			XpT = XpT[1:,:]
			YpT = YpT[1:]

		XTotI.append(XTotT)
		YTotI.append(YTotT)
		XpI.append(XpT)
		YpI.append(YpT)

	return XTotI, YTotI, XpI, YpI, XtestIT, YtestIT, XtestI, YtestI


XTotI, YTotI, XpI, YpI, XtestIT, YtestIT, XtestI, YtestI = generateIters(numCauses, numPredictors,numIters)

for iter in range(numIters):

	XTotT = XTotI[iter]
	YTotT = YTotI[iter]
	XpT = XpI[iter]
	YpT = YpI[iter]
	XtestT = XtestIT[iter]
	YtestT = YtestIT[iter]
	Xtest = XtestI[iter]
	Ytest = YtestI[iter]

	totTasks = 0

	for numTasks in tasksArray:

		XTot = XTotT[0:(numTasks)]
		YTot = YTotT[0:(numTasks)]

		Xp = XpT[0:numSamples*numTasks,:]
		Yp = YpT[0:numSamples*numTasks]

		beta = np.zeros(numPredictors)
		lam = 0.999

		par = params(XTot, lam, isPoly, kerPow)
		betaO = optimize.fmin_cg(lossFunctionPairwise, beta,args=(XTot,YTot,par),fprime=gradientPairwise,maxiter=iterations, disp = 0)

		par = params(XTot, 1., isPoly, kerPow, 10)
		indexInv = getInvariantSet(betaO,paramList,Xp,Yp,XTot,YTot,par)

		mask = np.ones(numPredictors, dtype=bool)
		mask[indexInv] = False
		indexNInv = np.arange(numPredictors)[mask]

		XC = XtestT[:,indexInv]
		XE = XtestT[:,indexNInv]

		folds = 10

		#GET FILES FOR MMD statistical test$
		if computeMMD:
			stT = 'mat/'+ str(numTasks)
			stI = str(iter)
			stringArray = ['matTrainResiduals','matTestResiduals','matTrainResidualsInv','matTestResidualsInv']
			stringArray = [stT + s + stI for s in stringArray]
			getMatricesForMMD(Xp,Yp,Xtest,Ytest,indexInv,paramList,stringArray,par,False,True)
			stringArray = ['matTrainResidualsLasso','matTestResidualsLasso']
			stringArray = [stT + s + stI for s in stringArray]
			getMatricesForMMD(Xp,Yp,Xtest,Ytest,indexInv,paramList,stringArray,par,True,False)

		i = 0
		ind = 0

		for samp in samplesTest:

			if samp > 0:
				XpIter = np.append(Xp, XtestT[0:samp,:],axis = 0)
				YpIter = np.append(Yp,YtestT[0:samp])
			else:
				XpIter = Xp
				YpIter = Yp


			# COMPUTE POOLED ESTIMATOR WITH RIDGE
			predPool, evP, alpha = fitAndPredict(XpIter,YpIter, Xtest, Ytest, paramList,False,folds)

			# COMPUTE POOLED ESTIMATOR WITH LASSO
			paramListLasso = [1e-10,1e-5,1e-3,0.1,1,10]
			predPoolLasso, evPlasso, alpha = fitAndPredict(XpIter,YpIter, Xtest, Ytest, paramListLasso,True,folds)

			# COMPUTE BETAINV

			pred, evI, alphaCEmp = fitAndPredict(XpIter[:,indexInv],YpIter, Xtest[:,indexInv], Ytest, paramList,False,folds)
			epsEst = np.mean((YpIter-np.sum(XpIter[:,indexInv]*alphaCEmp,1))**2)

			nC = alphaCEmp.size

			# COMPUTE BCAUS (GROUND TRUTH)
			indexCau = np.arange(numCauses)
			predCau, evCau, alpha = fitAndPredict(XpIter[:,indexCau],YpIter, Xtest[:,indexCau], Ytest, paramList,False,folds)

			nC = alphaCEmp.size

			if samp > 0:

				# COMPUTE DOMAIN INDEPENDENT
				predDom, evD, alpha = fitAndPredict(XtestT[0:samp,:],YtestT[0:samp], Xtest, Ytest, paramList,False,min(samp,10))

				# COMPUTE BETAINV+

				param = np.array([1e-10,1e-5,1e-3,1e-2,1e-1,0,1,10,100])
				best_reg, best_p = crossVal_infp(param,epsEst,XC[0:samp,:],XE[0:samp,:],YtestT[0:samp],alphaCEmp,samp,2)
				betaC, betaE = optimalCoefs(alphaCEmp,epsEst,XC[0:samp,:],XE[0:samp,:],YtestT[0:samp],samp,nC,numPredictors-nC,1e-5)
				predCool = np.sum(betaC*Xtest[:,indexInv],1) + np.sum(betaE*Xtest[:,indexNInv],1)
				evC = explainedVariance(predCool,Ytest)

			else:
				evC = 0
				evD = 0

			errorNumSamples[iter,ind,0,totTasks] = evP
			errorNumSamples[iter,ind,1,totTasks] = evI
			errorNumSamples[iter,ind,2,totTasks] = evC
			errorNumSamples[iter,ind,3,totTasks] = evD
			errorNumSamples[iter,ind,4,totTasks] = evCau
			errorNumSamples[iter,ind,5,totTasks] = evPlasso

			ind = ind+1

		totTasks = totTasks+1


labels = [r"$\beta^{poolR}$",r"$\beta^{inv}$",r"$\beta^{inv+}$",r"$\beta^{dom}$",r"$\beta^{cau}$",r"$\beta^{poolL}$"]

last = totTasks-1
fig, ax0 = plt.subplots(nrows=1, sharex=True)
ax0.errorbar(samplesTest, np.mean(errorNumSamples[:,:,0,last],axis=0), yerr= np.std(errorNumSamples[:,:,0,last],axis=0),color='#B80000', marker = 'o')
ax0.errorbar(samplesTest, np.mean(errorNumSamples[:,:,1,last],axis=0), yerr= np.std(errorNumSamples[:,:,1,last],axis=0),color='#E68A2E', marker = 'o')
ax0.errorbar(samplesTest, np.mean(errorNumSamples[:,:,2,last],axis=0), yerr= np.std(errorNumSamples[:,:,2,last],axis=0),color='#0066FF', marker = 'o')
ax0.errorbar(samplesTest, np.mean(errorNumSamples[:,:,3,last],axis=0), yerr= np.std(errorNumSamples[:,:,3,last],axis=0), color='#2EB82E', marker = 'o')
ax0.errorbar(samplesTest, np.mean(errorNumSamples[:,:,4,last],axis=0), yerr= np.std(errorNumSamples[:,:,4,last],axis=0), color='#D63083', marker = 'o')
ax0.errorbar(samplesTest, np.mean(errorNumSamples[:,:,5,last],axis=0), yerr= np.std(errorNumSamples[:,:,5,last],axis=0), color='k', marker = 'o')
ax0.legend(labels,loc='lower right')
pl.xlabel(r'Number of samples in the unseen domain')
pl.ylabel(r'Explained variance')

pl.savefig('syntSamples.eps', format='eps', dpi=1000)
pl.show()

labels = [r"$\beta^{poolR}$",r"$\beta^{inv}$",r"$\beta^{inv+}$",r"$\beta^{cau}$",r"$\beta^{poolL}$"]

fig, ax0 = plt.subplots(nrows=1, sharex=True)
ax0.errorbar(tasksArray, np.mean(errorNumSamples[:,-1,0,:],axis=0), yerr= np.std(errorNumSamples[:,-1,0,:],axis=0),color='#B80000', marker = 'o')
ax0.errorbar(tasksArray, np.mean(errorNumSamples[:,-1,1,:],axis=0), yerr= np.std(errorNumSamples[:,-1,1,:],axis=0),color='#E68A2E', marker = 'o')
ax0.errorbar(tasksArray, np.mean(errorNumSamples[:,-1,2,:],axis=0), yerr= np.std(errorNumSamples[:,-1,2,:],axis=0),color='#0066FF', marker = 'o')
ax0.errorbar(tasksArray, np.mean(errorNumSamples[:,-1,4,:],axis=0), yerr= np.std(errorNumSamples[:,-1,4,:],axis=0), color='#D63083', marker = 'o')
ax0.errorbar(tasksArray, np.mean(errorNumSamples[:,-1,5,:],axis=0), yerr= np.std(errorNumSamples[:,-1,5,:],axis=0), color='k', marker = 'o')
ax0.legend(labels,loc='lower right')
pl.ylabel(r'Explained variance')
pl.xlabel(r'Number of training tasks')
pl.savefig('syntTasks.eps', format='eps', dpi=1000)

pl.show()

with open(errors_file,'wb') as f:
	pickle.dump(errorNumSamples,f)
