# Copyright (c) 2014 - 2015  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import numpy as np
import scipy as sc
import scipy.io
import math as m
import sys
from sklearn import datasets, linear_model
from scipy import optimize
from sklearn.kernel_approximation import RBFSampler
from sklearn import cross_validation
from sklearn.cross_validation import KFold
from lossFunction import *
import pylab as P
from math import sqrt

from matplotlib import pyplot as pl
from matplotlib import rc
mpl.rc("figure",facecolor="white")
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

def sigmoid(x):
	sig = 1./(1.+np.exp(-x))
	assert(sig.all() >= 0 and sig.all()<=1)
	return sig

def optimalCoefs(alphaC,epsEst,Xc,Xe,Y,numSamples,numCauses,numEffects,reg):

	r = np.eye(numCauses)
	re = np.eye(numEffects)

	betaE, betaC = 0,alphaC
	params = np.array([1e-10,1e-5,1e-2,1e-1,0,1,10,100])
	"""Estimate alphaE"""
	regr = linear_model.Ridge(alpha = 0.01)

	regr.fit(Y[:,np.newaxis], Xe)

	try:

		alphaE_estim = regr.coef_.reshape(regr.coef_.size)

		"""Estimate matrices"""
		SigmaX_estimated = 1./numSamples*np.dot(Xc.T,Xc)


		SigmaXe_estimated = 1./numSamples*np.dot(Xc.T,(Xe-Y[:,np.newaxis]*alphaE_estim))
		Sigmae_estimated = 1./numSamples*np.dot((Xe-Y[:,np.newaxis]*alphaE_estim).T,(Xe-Y[:,np.newaxis]*alphaE_estim))

		sigma2_estimated = epsEst

		invX = np.linalg.inv(SigmaX_estimated + reg*r)
		M = sigma2_estimated*alphaE_estim[:,np.newaxis]*alphaE_estim
		M += Sigmae_estimated
		M -= np.dot(np.dot(invX,SigmaXe_estimated).T,SigmaXe_estimated)

		betaE = np.dot(sigma2_estimated*alphaE_estim,np.linalg.inv(M+reg*re).T)
		Mat = np.dot(invX,np.dot(SigmaXe_estimated,betaE[:,np.newaxis]))
		Mat = Mat.reshape(Mat.size)

		betaC = alphaC*(-np.dot(betaE,alphaE_estim)+1)- Mat

	except Exception as inst:
		print inst
		pass

	return betaC, betaE

def explainedVariance(p,t):
	assert(p.shape == t.shape)
	return 1-(np.mean((p-t)**2))/np.var(t)

def accuracy(p,t):
	assert(p.shape == t.shape)
	return 0.5*np.sum(np.abs(np.sign(p)-np.sign(t)))/t.size


def featureExpansion(X,Y,components,gammaV):
	rbf_feature = RBFSampler(gamma=gammaV, n_components = components, random_state=1)
	X_features = X# rbf_feature.fit_transform(X,Y)
	return X_features

def crossVal(paramList,X,Y,lasso,folds):

	best_score = 0
	best_reg = 0

	for re in paramList:
		if(lasso):
			regr = linear_model.Lasso(alpha = re)
		else:
			regr = linear_model.Ridge(alpha=re)
		scores = np.mean(cross_validation.cross_val_score(regr,X,Y,cv=folds))

		if scores > best_score:
			best_score = scores
			best_reg = re

	return best_reg, best_score

def crossVal_infp(paramList,epsEst,Xs,Xn,Y,alphaC,nsT,folds):

	kf = KFold(nsT,n_folds = folds)
	best_score = 0
	predCool = 0
	best_reg = 0

	for param in paramList:
		av_score = 0
		siz = 0.
		try:
			for train, test in kf:
				betaC, betaE = optimalCoefs(alphaC,epsEst,Xs[train,:],Xn[train,:],Y[train],Xs[train,:].shape[0],Xs.shape[1],Xn.shape[1],param)

				predCoolT = np.sum(betaC*Xs[test,:],1) + np.sum(betaE*Xn[test,:],1)
				predCoolT = predCoolT.flatten()
				a = explainedVariance(predCoolT,Y[test])
				av_score += a
				siz += 1.
			av_score = av_score/siz

			if av_score>best_score and av_score != 0:
				best_score= av_score
				best_reg = param

		except Exception as inst:
			print inst
			pass

	return best_reg, best_score

def domainCV(betaInit,X,Y,par,lambdGrid):

	maxScore = 100
	bestLambd = 0
	iterations = par.iterations_
	numDomains = par.numDomains_
	usedDom = np.arange(numDomains)
	par.setnumDomains(numDomains-1)

	for lambd in lambdGrid:

		score = 0
		ev = []
		for dom in range(numDomains):

			mask = np.ones(numDomains, dtype = bool)
			mask[dom] = False
			usedDomIter = usedDom[mask]
			par.setLambd(lambd)

			betaInv = optimize.fmin_cg(lossFunctionPairwise, betaInit,args=([X[i] for i in usedDomIter],[Y[i] for i in usedDomIter],par),fprime=gradientPairwise,maxiter=iterations, disp = 0)
			#predicted = np.dot(X[dom],betaInv[:,np.newaxis]).flatten()
			predicted = np.sum(betaInv*X[dom],1)
			target = Y[dom]
			#print explainedVariance(predicted,target)
			#score += explainedVariance(predicted,target)
			ev.append(explainedVariance(predicted,target))
		#score = score/numDomains
		score = np.var(np.array(ev))
		#print score

		if score < maxScore:
			maxScore = score
			bestLambd = lambd

	return bestLambd, maxScore

def compareParams(betaA,betaB):

	pl.scatter(betaA,betaB)
	x = np.arange(np.min(betaA)-0.000001,np.max(betaA)+0.00001,0.00001)
	pl.plot(x,x)
	pl.show()

def getInvariantSet(betaO,paramRange,X,Y,features,targets,params):

	folds = params.folds_
	targets = [t.flatten() for t in targets]

	maxTemp = lossFunctionPairwise(np.zeros(features[0].shape[1]),features,targets,params)

	sortBeta = np.argsort(np.abs(betaO))[::-1]
	indexC = 0

	for u in range(1,sortBeta.size-1,10):

		indexC_t = sortBeta[0:u]

		bestParam, s = crossVal(paramRange,X[:,indexC_t],Y,0,folds)
		regr = linear_model.Ridge(alpha=bestParam)
		regr.fit(X[:,indexC_t], Y)

		params.setPredictors(u)

		loss = lossFunctionPairwise(np.array(regr.coef_).flatten(),[f[:,indexC_t] for f in features],targets,params)

		if loss<maxTemp:
			indexC = indexC_t
			maxTemp = loss

	return indexC

def getSimilarity(betaLasso,indexInv):

	ts = np.zeros(betaLasso.size)

	for index in range(betaLasso.size):
		if(betaLasso[index] !=0):
			inBeta = np.where(indexInv == index)[0]
			#print inBeta
			if inBeta.size !=0:
				ts[index:] += 1

	return ts

def fitAndPredict(X,Y, Xtest, Ytest, paramList,withLasso,folds):

	best_reg, best_score = crossVal(paramList,X,Y,withLasso,folds)
	if withLasso:
		regr = linear_model.Lasso(alpha = best_reg)
	else:
		regr = linear_model.Ridge(alpha = best_reg)

	regr.fit(X,Y)
	pred = regr.predict(Xtest)
	ev = explainedVariance(pred,Ytest)
	alpha = regr.coef_

	return pred, ev, alpha


def medHeuristic(X):

	samples = X.shape[0]
	Z = np.zeros((samples,samples))

	for i in range(samples):
		for j in range(samples):
			Z[i,j] = np.linalg.norm(X[i,:]-X[j,:])
	median = np.median(Z)
	print Z
	print median

	return median**2


def plotResiduals(Y,pred,Yt,predt):

	titles = [r'Kitchen',r'Books',r'Electronics',r'DVDs']
	f,ax = pl.subplots(2,2)
	ax[0,0].hist(Y[0:2000].flatten()-pred[0:2000],15)
	ax[0,0].set_title(titles[0])
	ax[0,1].hist(Y[2000:4000].flatten()-pred[2000:4000],15)
	ax[0,1].set_title(titles[1])
	ax[1,0].hist(Y[4000:6000].flatten()-pred[4000:6000],15)
	ax[1,0].set_title(titles[2])
	ax[1,1].hist(Yt.flatten()-predt,15)
	ax[1,1].set_title(titles[3])
	pl.xlabel(r'Residuals')

	pl.savefig('histograms.eps', format='eps', dpi=1000)
	pl.show()


def crossValDomains(paramGrid,mu,features, targets,numTasks,numSamplesOpti,numPredictors,isPoly,ls,kerPow,iterations):

	cross = [[0,1],[1,2],[0,2]]
	targ = [2,0,1]

	beta = np.random.normal(0,0.01)

	best_score = 0
	best_param = 0

	for param in paramGrid:

		avgParam = 0

		for it in range(3):

			features_it = [features[u] for u in cross[it]]
			targets_it = [targets[u] for u in cross[it]]

			betaO = optimize.fmin_cg(lossFunctionPairwise, beta, args=(param,mu,[f[0:numSamplesOpti,:] for f in features_it],[t[0:numSamplesOpti] for t in targets_it],numTasks-1,numSamplesOpti,numPredictors,isPoly,ls,kerPow),fprime=gradientOneBeta,maxiter=iterations)


 # ------------- SAVE MATRICES WITH RESIDUALS FOR POOLED AND INV COEFFICIENTS IN ORDER TO DO MMD TEST ----------------

def getMatricesForMMD(X,Y,featuresTest,targetTest,indexC,paramGrid,stringArray,params, withLasso,withInv):

 	folds = params.folds_
 	numSamples = params.numSamples_

 	if withLasso:
		best_regInv, best_score = crossVal(paramGrid,X,Y,True,folds)
		regr = linear_model.Lasso(alpha = best_regInv)

	else:
		best_regInv, best_score = crossVal(paramGrid,X,Y,False,folds)
		regr = linear_model.Ridge(alpha = best_regInv)

	regr.fit(X,Y)
	betaPoolM = regr.coef_

	predPool =  regr.predict(featuresTest[0:numSamples,:]).flatten()
	ResTrain = (Y.flatten()-regr.predict(X).flatten())[:,np.newaxis]
	ResTest = (targetTest.flatten()[0:numSamples]-predPool)[:,np.newaxis]

	scipy.io.savemat(stringArray[0], mdict={'Residuals':ResTrain })
	scipy.io.savemat(stringArray[1], mdict = {'Residuals':ResTest})

	if withInv:
		best_regInv, best_score = crossVal(paramGrid,X[:,indexC],Y,False,10)
		regr = linear_model.Ridge(alpha = best_regInv)
		regr.fit(X[:,indexC],Y)
		alphaC = regr.coef_

		predC = regr.predict(featuresTest[0:numSamples,indexC]).flatten()
		ResTrain = (Y.flatten()-regr.predict(X[:,indexC]).flatten())[:,np.newaxis]
		ResTest = (targetTest.flatten()[0:numSamples]-predC)[:,np.newaxis]

		scipy.io.savemat(stringArray[2], mdict={'Residuals':ResTrain })
		scipy.io.savemat(stringArray[3], mdict = {'Residuals':ResTest})

#CLASSIFICATION

def classification_errorList(beta, X, Y):

	totScore = 0
	for t in range(len(X)):
		Ydom = Y[t].reshape(Y[t].size)
		Yhat = np.round(sigmoid(np.dot(beta,X[t].T)))
		R = Ydom - Yhat

		score = sum(abs(R))
		print score
		totScore += score/Y[t].size
	return totScore/float(len(X))

def classification_error(beta, X, Y):

	totScore = 0

	Y = Y.reshape(Y.size)
	Yhat = np.round(sigmoid(np.dot(beta,X.T)))
	R = Y - Yhat

	score = sum(abs(R))

	return score/Y.size


def LF_classify_optimal(beta,Xs,Xn,Y,numSamples,numCauses,numPredictors):

	betaS = beta[0:numCauses]
	betaN = beta[numCauses:]

	causalContrib = np.dot(betaS,Xs.T)
	noncContrib = np.dot(betaN,Xn.T)
	Yhat = causalContrib + noncContrib

	return -np.sum(Y*log(sigmoid(Yhat)+1e-10)+(1-Y)*log(1+1e-10-sigmoid(Yhat)))/numSamples

def Gradient_classify_optimal(beta,Xs,Xn,Y,numSamples,numCauses,numPredictors):

	gradient = np.zeros(numPredictors)

	betaS = beta[0:numCauses]
	betaN = beta[numCauses:]

	causalContrib = np.dot(betaS,Xs.T)
	noncContrib = np.dot(betaN,Xn.T)
	Yhat = causalContrib + noncContrib
	gradient[0:numCauses] = -np.dot(Y-sigmoid(Yhat),Xs)/numSamples
	gradient[numCauses:] = -np.dot(Y-sigmoid(Yhat),Xn)/numSamples

	return gradient

def getInvariantSet_classification(betaO,X,Y,features,targets,param):

	targets = [t.flatten() for t in targets]
	parInv = params(features, 1, 0, 0.1)
	maxTemp = lossFunctionClassify(np.zeros(features[0].shape[1]),features,targets,parInv)
	print maxTemp

	sortBeta = np.argsort(np.abs(betaO))[::-1]
	indexC = 0

	for u in range(1,sortBeta.size-1,1):
		print u
		indexC_t = sortBeta[0:u]

		#bestParam, s = crossVal(paramRange,X[:,indexC_t],Y,0,folds)
		#regr = linear_model.Ridge(alpha=bestParam)
		#regr.fit(X[:,indexC_t], Y)
		beta = np.random.normal(0,0.01,u)
		param.setPredictors(u)
		parInv.setPredictors(u)

		betaTemp = optimize.fmin_cg(lossFunctionClassify, beta,args=([f[:,indexC_t] for f in features],targets,param),fprime=gradientClassify,maxiter=1000, disp = 0)
		loss = lossFunctionClassify(betaTemp,[f[:,indexC_t] for f in features],targets,parInv)

		print loss
		if loss<maxTemp:
			indexC = indexC_t
			maxTemp = loss

	return indexC
