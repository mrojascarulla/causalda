from sklearn import datasets, linear_model
from scipy import optimize
from sklearn.kernel_approximation import RBFSampler
from matplotlib import pyplot as pl
from matplotlib import rc
import matplotlib as mpl
import pickle
import numpy as np

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

mpl.rc("figure",facecolor="white")

legends = [r"$\beta^{dom}$",r"$\beta^{inv+}$",r"$\beta^{poolR}$", r"$\beta^{inv}$",r"$\beta^{poolL}$"]
colors = ['#cc3333','#009966','#3333cc']
colors = ['MidnightBlue','FireBrick','Teal','r','g']
#colors =[ pl.get_cmap(c) for c in colors]
fi,ax = pl.subplots(2,2)

index = [(0,0),(0,1),(1,0),(1,1)]
titles = [r'Kitchen', r'Books', r'Electronics', r'DVDs']
ranges = [[0,0.32],[0,0.25],[0,0.3],[0,0.23]]

for indexT in range(4):
    #ReadErrors = 'errorsTestRepr'
    ReadErrors = 'errorsTestRepr' +str(indexT)
    with open(ReadErrors, 'rb') as f:
        errors = pickle.load(f)
        for u in range(1,6):
            ax[index[indexT]].plot(errors[0,:],errors[u,:].T,marker = 'o',markersize=3,color = colors[u-1])
        ax[index[indexT]].legend(legends,loc = 'lower right',prop={'size':8})
        ax[index[indexT]].set_title(titles[indexT])
        ax[index[indexT]].set_xlabel(r"Number of examples")
        ax[index[indexT]].set_ylabel(r"Explained variance")
        ax[index[indexT]].set_ylim(ranges[indexT])
        ax[index[indexT]].set_yticks(np.arange(0,0.3,0.05))

pl.tight_layout()
pl.savefig('plots.eps', format='eps', dpi=1000)

pl.show()
