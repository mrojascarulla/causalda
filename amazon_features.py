# Copyright (c) 2014 - 2015  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import scipy.io
import scipy.optimize as sco
from xml.dom import minidom
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
from bs4 import NavigableString
import codecs
import nltk as nltk
from sklearn.svm import SVR
import numpy as np
import sys
import pickle
from sklearn.kernel_ridge import KernelRidge

# ----------------------------------------------------------------------------------------------------

#       COMPUTE AND SAVE THE FEATURES AND TARGETS FROM RAW XML DATA FOR THE AMAZON SENTIMENT DATASET

# ----------------------------------------------------------------------------------------------------


numWords = 5000

books_pos = open('amazonData/books/positive.review','r')
books_neg = open('amazonData/books/negative.review','r')
dvd_pos = open('amazonData/dvd/positive.review','r')
dvd_neg = open('amazonData/dvd/negative.review','r')
elec_pos = open('amazonData/electronics/positive.review','r')
elec_neg = open('amazonData/electronics/negative.review','r')
kit_pos = open('amazonData/kit/positive.review','r')
kit_neg = open('amazonData/kit/negative.review','r')

stBP = '<root>' + books_pos.read() + '</root>'
stBN = '<root>' + books_neg.read() +'</root>'
stDP = '<root>' + dvd_pos.read() +'</root>'
stDN = '<root>' + dvd_neg.read() +'</root>'
stEP = '<root>' + elec_pos.read() +'</root>'
stEN = '<root>' + elec_neg.read() +'</root>'
stKP = '<root>' + kit_pos.read() +'</root>'
stKN = '<root>' + kit_neg.read() +'</root>'

BP=BeautifulSoup(stBP)
BN=BeautifulSoup(stBN)

DP=BeautifulSoup(stDP)
DN=BeautifulSoup(stDN)

EP=BeautifulSoup(stEP)
EN=BeautifulSoup(stEN)

KP=BeautifulSoup(stKP)
KN=BeautifulSoup(stKN)

Reviews = [BP,BN,DP,DN,EP,EN,KP,KN]

num = 0

total_text = ''

for rev in Reviews:

	a_data = rev.find_all("review")

	for item in a_data:
		total_text += item.find_all("review_text")[0].text

total_text.encode("utf-8",'ignore')

unigram = nltk.tokenize.word_tokenize(total_text)
bigram = nltk.bigrams(unigram)

fdist = nltk.FreqDist(unigram).most_common(numWords)
fbigram = nltk.FreqDist(bigram).most_common(numWords)

X_features = []
Y_targets = []

for i in range(0,8,2):

	print i
	X_dom = np.empty((1,2*numWords))
	Y_dom = np.empty((1,1))
	rev_p = Reviews[i]
	rev_n = Reviews[i+1]

	pos_data = rev_p.find_all("review")
	neg_data = rev_n.find_all("review")

	normalize_words = 1
	normalize_bigrams = 1

	for item in (pos_data+neg_data):

		text = item.find_all("review_text")[0].text
		token_text = nltk.tokenize.word_tokenize(text)
		bigram_text = nltk.bigrams(token_text)
		fdist_text = nltk.FreqDist(token_text)
		fbigram_text = nltk.FreqDist(bigram_text)

		X = []
		target = nltk.tokenize.word_tokenize(item.find_all("rating")[0].text)

		target = float(target[0])

		for lelem in fdist:

			try:
				X.append(float(fdist_text[lelem[0]]))
			except Exception:
				X.append(0)

		for lelem in fbigram:

			try:
				X.append(float(fbigram_text[lelem[0]]))
			except Exception:
				X.append(0)

		X_dom = np.append(X_dom,np.array(X)[np.newaxis,:],axis=0)
		Y_dom = np.insert(Y_dom,-1,target)[:,np.newaxis]

	X_dom[:,0:numWords] = X_dom[:,0:numWords]/normalize_words
	X_dom[:,numWords:] = X_dom[:,numWords:]/normalize_bigrams
	X_features.append(X_dom[1:,:])
	Y_targets.append(Y_dom[1:,:])

fileFeatures_name = 'amazonData/features'
fileTargets_name = 'amazonData/targets'

with open(fileFeatures_name,'wb') as f:
	pickle.dump(X_features,f)


with open(fileTargets_name,'wb') as f:
	pickle.dump(Y_targets,f)
