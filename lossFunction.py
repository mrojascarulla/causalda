# Copyright (c) 2014 - 2015  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import numpy as np
import math as m

import scipy as sc
import scipy.weave as weave
from scipy.weave import converters
from scipy import optimize

import random
import sys

from pylab import *
from matplotlib import pyplot as pl

#from utils import *

def sigmoid(x):
	sig = 1./(1.+np.exp(-x))
	assert(sig.all() >= 0 and sig.all()<=1)
	return sig

class params:

	def __init__(self, X, lambd, isPoly, kernelParam,folds = None, iterations = None):

		#if not(assert(X[0].shape[0] == Y[0].shape[0])):
		#	raise Exception("Inconsistent number of samples")
		self.numSamples_ = X[0].shape[0]
		self.numPredictors_ = X[0].shape[1]
		self.numDomains_ = len(X)
		self.kernelType_ = isPoly
		self.kernelParam_ = kernelParam
		self.lambd_ = lambd
		self.folds_ = folds or 0
		self.iterations_ = iterations or 1000

	def setPredictors(self,numPredictors):
		self.numPredictors_ = numPredictors
	def setLambd(self,lambd):
		self.lambd_ = lambd
	def setnumDomains(self,numDomains):
		self.numDomains_ = numDomains

def lossFunctionPairwise(beta,X,Y,params):

	numSamples = params.numSamples_
	numPredictors = params.numPredictors_
	numDomains = params.numDomains_
	isPoly = params.kernelType_
	kernelParam = params.kernelParam_
	lambd = params.lambd_

	loss = 0.
	Res = []

	for dom in range(numDomains):

		Ydom = Y[dom].reshape(Y[dom].size)

		Yhat = np.dot(beta,X[dom].T)

		Res.append(Ydom-Yhat)

		normYdif = np.linalg.norm(Res[-1])**2/numSamples
		loss += (1.-lambd)*normYdif

	Res = np.array(Res).flatten()

	if(lambd != 0):

		support = "#include <math.h>"
		code = """

		float totReturn1 = 0;
		float totReturn2 = 0;
		float totReturn3 = 0;
		float normDiff = 0;

		float norm1 = 1./(numSamples*(numSamples-1));
		float norm2 = 1./(numSamples*numSamples);

		for(int dom = 0; dom<numDomains; dom++){

			float Q1 = 0;
			float Q2 = 0;
			float Q3 = 0;

			for(int i = 0; i<numSamples; i++){

				int index_i = i + dom*numSamples;
				for(int j = 0; j<numSamples; j++){
					if(isPoly){
						Q1 += pow((Res(index_i)*Res(j+dom*numSamples))+1,kernelParam);
						}
					else
						Q1 += exp(-pow((Res(index_i)-Res(j+dom*numSamples)),2)/kernelParam);

					for(int dom2 = 0; dom2<numDomains; dom2++){

						if (dom2 != dom){
							if (isPoly){
								Q2 += pow((Res(i+dom2*numSamples)*Res(j+dom2*numSamples))+1,kernelParam);
								Q3 += pow((Res(index_i)*Res(j+dom2*numSamples))+1,kernelParam);
							}
							else {
								Q2 += exp(-pow((Res(i+dom2*numSamples)-Res(j+dom2*numSamples)),2)/kernelParam);
								Q3 += exp(-pow((Res(index_i)-Res(j+dom2*numSamples)),2)/kernelParam);
							}
						}
					}
				}

			}

			totReturn1 += Q1;
			totReturn2 += Q2;
			totReturn3 -= 2*Q3;
		}

		float ret = totReturn1*(numDomains-1)*norm1 + totReturn2*norm1 + totReturn3*norm2;

		return_val = ret;

		"""
		# Call weave to fill in phi
		output = weave.inline(code,['numSamples','numDomains','Res','isPoly','kernelParam'],
		type_converters = converters.blitz,
		support_code = support,libraries = ['m'],compiler = 'gcc')

		emb = lambd/float(numDomains)**2*output
		loss += emb

	return loss

def gradientPairwise(beta,X,Y,params):

	numSamples = params.numSamples_
	numPredictors = params.numPredictors_
	numDomains = params.numDomains_
	isPoly = params.kernelType_
	kernelParam = params.kernelParam_
	lambd = params.lambd_
	gradient = np.zeros(numPredictors)

	Res = []

	for dom in range(numDomains):
		Ydom = Y[dom].reshape(Y[dom].size)
		Yhat = np.dot(beta,X[dom].T)
		Res.append(Ydom-Yhat)
		gradient += -2.*(1.-lambd)*np.dot(Res[-1],X[dom])/numSamples

	Rescp = Res
	Res = np.array(Res).flatten()
	Xb = np.array(X).flatten()

	if lambd != 0:
		support = "#include <math.h>"
		code = """

		float totReturn1 = 0;
		float totReturn2 = 0;
		float totReturn3 = 0;
		float der0ker = 0;
		float der2ker = 0;

		float norm2 = 1./(numSamples*numSamples);
		float norm1 = 1./(numSamples*(numSamples-1));

		for(int dom = 0; dom<numDomains; dom++){

			float Q1[10001];
			float Q2[10001];
			float Q3[10001];

			for(int s = 0; s<numPredictors; s++){
				Q1[s] = 0;
				Q2[s] = 0;
				Q3[s] = 0;
			}

			for(int i = 0; i<numSamples; i++){
				for(int j = 0; j<numSamples; j++){

					if(isPoly)
						der0ker = kernelParam*pow((Res(i+dom*numSamples)*Res(j+dom*numSamples))+1,kernelParam-1);
					else {
						der0ker = exp(-pow((Res(i+dom*numSamples)-Res(j+dom*numSamples)),2)/kernelParam);
						der0ker = -2/kernelParam*(Res(i+dom*numSamples)-Res(j+dom*numSamples))*der0ker;
					}

					for(int s = 0; s<numPredictors; s++) {
						if (isPoly){
								Q1[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i + s)*Res(j+dom*numSamples) + Xb(numPredictors*dom*numSamples + numPredictors*j+s)*Res(i+dom*numSamples));
							}

						else
							Q1[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i + s) - Xb(numPredictors*dom*numSamples + numPredictors*j+s));
						}

					}
				for(int j=0; j<numSamples; j++){
					for(int dom2= 0; dom2<numDomains; dom2++){
						if(dom2 != dom){
							if(isPoly){
								der0ker = kernelParam*pow((Res(i+dom*numSamples)*Res(j+dom2*numSamples))+1,kernelParam-1);
							}
							else{
								der0ker = exp(-pow((Res(i+dom*numSamples)-Res(j+dom2*numSamples)),2)/kernelParam);
								der0ker *= -2/kernelParam*(Res(i+dom*numSamples)-Res(j+dom2*numSamples));
							}

							if( isPoly)
									der2ker = kernelParam*pow((Res(i+dom2*numSamples)*Res(j+dom2*numSamples))+1,kernelParam-1);
								else{
									der2ker = exp(-pow((Res(i+dom2*numSamples)-Res(j+dom2*numSamples)),2)/kernelParam);
									der2ker = -2/kernelParam*(Res(i+dom2*numSamples)-Res(j+dom2*numSamples))*der2ker;
									}

							for(int s=0; s<numPredictors; s++){
								if(isPoly){
									Q2[s] -= der2ker*(Xb(numPredictors*dom2*numSamples + numPredictors*i + s)*Res(j+dom2*numSamples) + Xb(numPredictors*dom2*numSamples + numPredictors*j+s)*Res(i+dom2*numSamples));
									Q3[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i +s)*Res(j+dom2*numSamples)+ Xb(numPredictors*dom2*numSamples + numPredictors*j +s)*Res(i+dom*numSamples));
								}
								else{
									Q2[s] -= der2ker*(Xb(numPredictors*dom2*numSamples + numPredictors*i + s) - Xb(numPredictors*dom2*numSamples + numPredictors*j+s));
									Q3[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i +s)- Xb(numPredictors*dom2*numSamples + numPredictors*j +s));
								}
								}
							}

						}
					}

				}

			for(int s = 0; s<numPredictors; s++)
				gradient(s) += lambd/pow(float(numDomains),2)*((numDomains-1)*Q1[s]*norm1 +Q2[s]*norm1 - 2*Q3[s]*norm2);
		}

		"""

		weave.inline(code,['beta','numSamples','numDomains','numPredictors','gradient','Res','Xb','lambd','isPoly','kernelParam'],
		type_converters = converters.blitz,
		support_code = support,libraries = ['m'])

	return gradient


def lossFunctionPool(beta,X,Y,params):

	loss = 0.
	Res = []

	numSamples = params.numSamples_
	numPredictors = params.numPredictors_
	numDomains = params.numDomains_
	isPoly = params.kernelType_
	kernelParam = params.kernelParam_
	lambd = params.lambd_


	for dom in range(numDomains):

		Ydom = Y[dom].reshape(Y[dom].size)
		Yhat = np.dot(beta,X[dom].T)

		Res.append(Ydom-Yhat)

		normYdif = np.linalg.norm(Res[-1])**2/numSamples
		loss += (1.-lambd)*normYdif

	Res = np.array(Res).flatten()

	if(lambd != 0):
		support = "#include <math.h>"
		code = """

		float totReturn1 = 0;
		float totReturn2 = 0;
		float totReturn3 = 0;
		float normDiff = 0;

		float norm3 = 1./(numSamples*numSamples*(numSamples-1));
		float norm2 = 1./pow((numSamples*(numSamples-1)),2);
		float norm1 = 1./pow(numSamples,2);

		for(int dom = 0; dom<numDomains; dom++){

			float Q1 = 0;
			float Q2 = 0;
			float Q3 = 0;


			for(int i = 0; i<numSamples; i++){

				int index_i = i + dom*numSamples;
				for(int j = 0; j<numSamples; j++){

					int index_j = j + dom*numSamples;
					if(isPoly){
						Q1 += pow((Res(index_i)*Res(index_j)),kernelParam);
					}
					else{
						Q1 += exp(-pow((Res(index_i)-Res(index_j)),2)/kernelParam);
					}
				}

				int j=0;
				if (dom == 0)
					j = numSamples;

				while((j<dom*numSamples || j>=(dom+1)*numSamples) && j<(numDomains)*numSamples){

					if (isPoly){
						Q3 += pow((Res(index_i)*Res(j)),kernelParam);
					}
					else {
						Q3 += exp(-pow((Res(index_i)-Res(j)),2)/kernelParam);
					}

					if(j == dom*numSamples-1)
						j = (dom+1)*numSamples;
					else
						j++;
				}
			}


			int i = 0;

			if (dom == 0)
				i = numSamples;

			while((i<dom*numSamples || i>=(dom+1)*numSamples) && i<(numDomains)*numSamples){

				int j=0;
				if (dom == 0)
					j = numSamples;

				while((j<dom*numSamples || j>=(dom+1)*numSamples) && j<(numDomains)*numSamples){

							if (isPoly){
								Q2 += pow((Res(i)*Res(j)),kernelParam);
							}
							else {
								Q2 += exp(-pow((Res(i)-Res(j)),2)/kernelParam);
							}
							if(j == dom*numSamples-1)
								j = (dom+1)*numSamples;
							else
								j++;
						}

					if(i == dom*numSamples-1)
								i = (dom+1)*numSamples;
							else
								i++;
			}


			totReturn1 += Q1;
			totReturn2 += Q2;
			totReturn3 -= 2*Q3;
		}

		float ret = totReturn1*norm1 + totReturn2*norm2 + totReturn3*norm3;

		return_val = ret;

		"""
		# Call weave to fill in phi
		output = weave.inline(code,['numSamples','numDomains','Res','isPoly','kernelParam'],
		type_converters = converters.blitz,
		support_code = support,libraries = ['m'],compiler = 'gcc')

		emb = lambd/float(numDomains)**2*output
		loss += emb

	return loss


def gradientPool(beta,X,Y,params):

	Res = []

	numSamples = params.numSamples_
	numPredictors = params.numPredictors_
	numDomains = params.numDomains_
	isPoly = params.kernelType_
	kernelParam = params.kernelParam_
	lambd = params.lambd_
	gradient = np.zeros(numPredictors)

	for dom in range(numDomains):
		Ydom = Y[dom].reshape(Y[dom].size)

		Yhat = np.dot(beta,X[dom].T)
		Res.append(Ydom-Yhat)

		gradient += -2.*(1.-lambd)*np.dot(Res[-1],X[dom])/numSamples

	Rescp = Res
	Res = np.array(Res).flatten()
	Xb = np.array(X).flatten()

	if lambd != 0:
		support = "#include <math.h>"
		code = """

		float totReturn1 = 0;
		float totReturn2 = 0;
		float totReturn3 = 0;
		float der0ker = 0;
		float der2ker = 0;

		float norm3 = 1./(numSamples*numSamples*(numSamples-1));
		float norm2 = 1./pow((numSamples*(numSamples-1)),2);
		float norm1 = 1./pow(numSamples,2);

		for(int dom = 0; dom<numDomains; dom++){

			float Q1[10000];
			float Q2[10000];
			float Q3[10000];

			for(int s = 0; s<numPredictors; s++){
				Q1[s] = 0;
				Q2[s] = 0;
				Q3[s] = 0;
			}

			for(int i = 0; i<numSamples; i++){

				int index_i = i + dom*numSamples;
				for(int j = 0; j<numSamples; j++){
					int index_j = j + dom*numSamples;
					if(isPoly){
						der0ker = kernelParam*pow((Res(index_i)*Res(index_j)),kernelParam-1);
					}
					else{
						der0ker = exp(-pow((Res(index_i)-Res(index_j)),2)/kernelParam);
						der0ker *= -2/kernelParam*(Res(index_i)-Res(index_j));
					}
					for(int s = 0; s<numPredictors; s++) {
						if (isPoly){
							Q1[s] -= der0ker*(Xb(numPredictors*index_i + s)*Res(index_j) + Xb(numPredictors*index_j + s)*Res(index_i));
						}

						else
							Q1[s] -= der0ker*(Xb(numPredictors*index_i + s) - Xb(numPredictors*index_j+s));
					}
				}

				int j=0;
				if (dom == 0)
					j = numSamples;
				while((j<dom*numSamples || j>=(dom+1)*numSamples) && j<(numDomains)*numSamples){

					if (isPoly){
						der0ker = kernelParam*pow((Res(index_i)*Res(j)),kernelParam-1);
					}
					else {
						der0ker = exp(-pow((Res(index_i)-Res(j)),2)/kernelParam);
						der0ker *= -2/kernelParam*(Res(index_i)-Res(j));
					}
					for(int s = 0; s<numPredictors; s++) {

						if (isPoly){
							Q3[s] -= der0ker*(Xb(numPredictors*index_i + s)*Res(j) + Xb(numPredictors*j + s)*Res(index_i));
						}

						else
							Q3[s] -= der0ker*(Xb(numPredictors*index_i + s) - Xb(numPredictors*j+s));
					}

					if(j == dom*numSamples-1)
						j = (dom+1)*numSamples;
					else
						j++;
				}
			}


			int i = 0;

			if (dom == 0)
				i = numSamples;

			while((i<dom*numSamples || i>=(dom+1)*numSamples) && i<(numDomains)*numSamples){

				int j=0;
				if (dom == 0)
					j = numSamples;

				while((j<dom*numSamples || j>=(dom+1)*numSamples) && j<(numDomains)*numSamples){

					if (isPoly){
						der0ker = kernelParam*pow((Res(i)*Res(j)),kernelParam-1);
					}
					else {
						der0ker = exp(-pow((Res(i)-Res(j)),2)/kernelParam);
						der0ker *= -2/kernelParam*(Res(i)-Res(j));
					}
					for(int s = 0; s<numPredictors; s++) {

						if (isPoly){
							Q2[s] -= der0ker*(Xb(numPredictors*i + s)*Res(j) + Xb(numPredictors*j + s)*Res(i));
						}

						else
							Q2[s] -= der0ker*(Xb(numPredictors*i + s) - Xb(numPredictors*j+s));
					}

					if(j == dom*numSamples-1)
						j = (dom+1)*numSamples;
					else
						j++;
				}

				if(i == dom*numSamples-1)
					i = (dom+1)*numSamples;
				else
					i++;
			}


			for(int s = 0; s<numPredictors; s++)
				gradient(s) += lambd/pow(float(numDomains),2)*(Q1[s]*norm1 +Q2[s]*norm2 - 2*Q3[s]*norm3);
			}

		"""

		weave.inline(code,['beta','numSamples','numDomains','numPredictors','gradient','Res','Xb','lambd','isPoly','kernelParam'],
		type_converters = converters.blitz,
		support_code = support,libraries = ['m'])

	return gradient


def lossFunctionClassify(beta,X,Y,params):

	numSamples = params.numSamples_
	numPredictors = params.numPredictors_
	numDomains = params.numDomains_
	isPoly = params.kernelType_
	kernelParam = params.kernelParam_
	lambd = params.lambd_

	loss = 0.
	Res = []

	#Precompute the residuals for effiency
	for dom in range(numDomains):

		Ydom = Y[dom].reshape(Y[dom].size)
		Yhat = np.dot(beta,X[dom].T)

		Res.append(Ydom-sigmoid(Yhat))

		#normYdif = np.linalg.norm(Res[-1])**2/numSamples
		normYdif = -np.sum(Ydom*log(sigmoid(Yhat)+1e-10)+(1-Ydom)*log(1-sigmoid(Yhat)+1e-10))/numSamples

		loss += (1.-lambd)*normYdif

	Res = np.array(Res).flatten()


	if(lambd != 0):
		support = "#include <math.h>"
		code = """

		float totReturn1 = 0;
		float totReturn2 = 0;
		float totReturn3 = 0;
		float normDiff = 0;

		float norm1 = 1./(numSamples*(numSamples-1));
		float norm2 = 1./(numSamples*numSamples);

		for(int dom = 0; dom<numDomains; dom++){

			float Q1 = 0;
			float Q2 = 0;
			float Q3 = 0;

			for(int i = 0; i<numSamples; i++){

				int index_i = i + dom*numSamples;
				for(int j = 0; j<numSamples; j++){
					if(isPoly){
						Q1 += pow((Res(index_i)*Res(j+dom*numSamples)),kernelParam);
						}
					else
						Q1 += exp(-pow((Res(index_i)-Res(j+dom*numSamples)),2)/kernelParam);

					for(int dom2 = 0; dom2<numDomains; dom2++){

						if (dom2 != dom){
							if (isPoly){
								Q2 += pow((Res(i+dom2*numSamples)*Res(j+dom2*numSamples)),kernelParam);
								Q3 += pow((Res(index_i)*Res(j+dom2*numSamples)),kernelParam);
							}
							else {
								Q2 += exp(-pow((Res(i+dom2*numSamples)-Res(j+dom2*numSamples)),2)/kernelParam);
								Q3 += exp(-pow((Res(index_i)-Res(j+dom2*numSamples)),2)/kernelParam);
							}
						}
					}
				}

			}

			totReturn1 += Q1;
			totReturn2 += Q2;
			totReturn3 -= 2*Q3;

		}

		float ret = totReturn1*(numDomains-1)*norm1 + totReturn2*norm1 + totReturn3*norm2;
		return_val = ret;

		"""
		# Call weave to fill in phi
		output = weave.inline(code,['numSamples','numDomains','Res','isPoly','kernelParam'],
		type_converters = converters.blitz,
		support_code = support,libraries = ['m'],compiler = 'gcc')

		emb = lambd/float(numDomains)**2*output
		loss += emb

	return loss

def gradientClassify(beta,X,Y,params):

	numSamples = params.numSamples_
	numPredictors = params.numPredictors_
	numDomains = params.numDomains_
	isPoly = params.kernelType_
	kernelParam = params.kernelParam_
	lambd = params.lambd_

	gradient = np.zeros(numPredictors)

	Res = []
	Yhats = []

	for dom in range(numDomains):
		Ydom = Y[dom].reshape(Y[dom].size)
		Yhat = np.dot(beta,X[dom].T)
		Res.append(Ydom-sigmoid(Yhat))
		Yhats.append(sigmoid(Yhat))

		#gradient += -2.*(1.-lambd)*np.dot(Res[-1],(X[dom].T*sigmoid(Yhat)*(1-sigmoid(Yhat))).T)/numSamples
		gradient += -np.dot(Res[-1],X[dom])/numSamples

	Rescp = Res
	Res = np.array(Res).flatten()
	Yhats = np.array(Yhats).flatten()
	Xb = np.array(X).flatten()

	if lambd != 0:
		support = "#include <math.h>"
		code = """
		float totReturn1 = 0;
		float totReturn2 = 0;
		float totReturn3 = 0;
		float der0ker = 0;
		float der2ker = 0;

		float norm2 = 1./(numSamples*numSamples);
		float norm1 = 1./(numSamples*(numSamples-1));

		for(int dom = 0; dom<numDomains; dom++){

			float Q1[2000];
			float Q2[2000];
			float Q3[2000];

			for(int s = 0; s<numPredictors; s++){
				Q1[s] = 0;
				Q2[s] = 0;
				Q3[s] = 0;
			}

			for(int i = 0; i<numSamples; i++){
				for(int j = 0; j<numSamples; j++){

					if(isPoly)
						der0ker = kernelParam*pow((Res(i+dom*numSamples)*Res(j+dom*numSamples)),kernelParam-1);
					else {
						der0ker = exp(-pow((Res(i+dom*numSamples)-Res(j+dom*numSamples)),2)/kernelParam);
						der0ker = -2/kernelParam*(Res(i+dom*numSamples)-Res(j+dom*numSamples))*der0ker;
					}
					float sigj = Yhats(j+dom*numSamples);
					float sigjder = sigj*(1.-sigj);
					float sigi = Yhats(i+dom*numSamples);
					float sigider = sigi*(1.-sigi);

					for(int s = 0; s<numPredictors; s++) {
						if (isPoly){
								Q1[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i + s)*sigider*Res(j+dom*numSamples) + Xb(numPredictors*dom*numSamples + numPredictors*j+s)*sigjder*Res(i+dom*numSamples));
							}

						else
							Q1[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i + s)*sigider + Xb(numPredictors*dom*numSamples + numPredictors*j+s)*sigjder);

						}



					for(int dom2= 0; dom2<numDomains; dom2++){
						if(dom2 != dom){
							if(isPoly){
								der0ker = kernelParam*pow((Res(i+dom*numSamples)*Res(j+dom2*numSamples)),kernelParam-1);
							}
							else{
								der0ker = exp(-pow((Res(i+dom*numSamples)-Res(j+dom2*numSamples)),2)/kernelParam);
								der0ker *= -2/kernelParam*(Res(i+dom*numSamples)-Res(j+dom2*numSamples));
							}

							if( isPoly)
									der2ker = kernelParam*pow((Res(i+dom2*numSamples)*Res(j+dom2*numSamples)),kernelParam-1);
								else{
									der2ker = exp(-pow((Res(i+dom2*numSamples)-Res(j+dom2*numSamples)),2)/kernelParam);
									der2ker = -2/kernelParam*(Res(i+dom2*numSamples)-Res(j+dom2*numSamples))*der2ker;
									}

							float sigj = Yhats(j+dom2*numSamples);
							float sigjder = sigj*(1.-sigj);
							float sigi2 = Yhats(i+dom2*numSamples);
							float sigi2der = sigi2*(1.-sigi2);
							float sigi = Yhats(i+dom*numSamples);
							float sigider = sigi*(1.-sigi);

							for(int s=0; s<numPredictors; s++){
								if(isPoly){
									Q2[s] -= der2ker*(Xb(numPredictors*dom2*numSamples + numPredictors*i + s)*sigi2der*Res(j+dom2*numSamples) + Xb(numPredictors*dom2*numSamples + numPredictors*j+s)*sigjder*Res(i+dom2*numSamples));
									Q3[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i +s)*sigider*Res(j+dom2*numSamples)+ Xb(numPredictors*dom2*numSamples + numPredictors*j +s)*sigjder*Res(i+dom*numSamples));
								}
								else{
									Q2[s] -= der2ker*(Xb(numPredictors*dom2*numSamples + numPredictors*i + s)*sigi2der - Xb(numPredictors*dom2*numSamples + numPredictors*j+s)*sigjder);
									Q3[s] -= der0ker*(Xb(numPredictors*dom*numSamples + numPredictors*i +s)*sigider- Xb(numPredictors*dom2*numSamples + numPredictors*j +s)*sigjder);
									}
								}
							}

						}

					}
				}

			for(int s = 0; s<numPredictors; s++)
				gradient(s) += lambd/pow(float(numDomains),2)*((numDomains-1)*Q1[s]*norm1 + Q2[s]*norm1 - 2*Q3[s]*norm2);
		}
		"""

		weave.inline(code,['beta','numSamples','numDomains','numPredictors','gradient','Res','Xb','lambd','isPoly','kernelParam','Yhats'],
		type_converters = converters.blitz,
		support_code = support,libraries = ['m'])

	return gradient
