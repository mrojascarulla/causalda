# Copyright (c) 2014 - 2015  Mateo Rojas-Carulla  [mr597@cam.ac.uk]
# All rights reserved.  See the file COPYING for license terms.

import scipy.io
import scipy.optimize as sco
from xml.dom import minidom
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
from bs4 import NavigableString
import codecs
import nltk as nltk
from sklearn.svm import SVR
from sklearn import cross_validation
from sklearn.cross_validation import KFold
import numpy as np
import sys
import pickle
from sklearn.kernel_ridge import KernelRidge
from lossFunction import *
from utils import *
import itertools


# ----------------------------------------------------------------------------------------------------

#       ANALYSIS OF THE AMAZON DATASET EXAMPLE

# Save explained variance for different methods in 'errors'

# computeMMD: if set to True will save the matrices of residuals in the mat/ folder
# save: set to 0 if betaInv has already been computed and stored in a file (done automatically
# the first time it is computed).
# paramGrid: grid of parameters for cross validation
# samplesTestArr: number of seen labeled examples for the test task

# ----------------------------------------------------------------------------------------------------

np.random.seed(42)

numTasks = 3
iterations = 200
computeMMD = False

errors_file = 'errorsTestReprN'
save = 1

numSamples = 300

numPredictors = 1000

featuresTest = 0
targetTest = 0

samplesTest = 500

p = int(0.5*numPredictors)
s = int(0.5*numSamples)
st = int(0.5*samplesTest)

# --------------- LOAD THE DATA -----------------

fileTargets_name = '../../causalTransferLearning/causaltransferlearning/amazonData/targets'
fileFeatures_name = '../../causalTransferLearning/causaltransferlearning/amazonData/features'
#fileTargets_name = '../PhD-Code/amazonData/targets'
#fileFeatures_name = '../PhD-Code/amazonData/features'

# --------------- Index for 4 possible scenarios ---------------

indexTasks = [[0,1,2,3], [1,2,3,0], [0,1,3,2],[0,2,3,1]]
indexTest = [3,0,2]

# --------------- Grid for cross-validation ---------------

paramGrid =np.array([1e-5,1e-20,1e-10,0.001,0.01,0.1,1,10,100,500,1000,5000,10000,50000,100000,500000])
samplesTestArr = np.array([10,30,40,50,80,100,150,200,300,400,500,600,700,800,900,1000])

# --------------- Four possible combinations of (3 training tasks, 1 test task) ---------------

for indexT in range(4):

	with open(fileTargets_name,'rb') as f:
		targets = pickle.load(f)

	with open(fileFeatures_name,'rb') as f:
		features = pickle.load(f)

	features = [features[u] for u in indexTasks[indexT]]
	targets = [targets[u] for u in indexTasks[indexT]]
	#targets = []

	#--------------- Indexing variables for selecting the desired number of samples and predictors ---------------

	index = np.arange(2*p)
	indexSamples = np.append(np.arange(s),np.arange(1000,1000+s))
	indexSamplesTest = np.append(np.arange(st),np.arange(1000,1000+st))

	for i in range(3):

		features[i] = features[i][indexSamples,:]
		features[i] = features[i][:,index]
		targets[i] = targets[i][indexSamples,:]

	features[3] = features[3][:,index]

	#--------------- Normalize the features to have unit std and targets to have zero mean ---------------

	features = [f/(np.std(f,axis=0)+0.0000000000000001) for f in features]
	targets = [t-np.mean(t) for t in targets]

	temp = []

	for i in range(4):
		temp.append(np.append(features[i],targets[i],axis = 1))

	#--------------- Randomly shuffle the samples to mix positive and negative examples ---------------

	[np.random.shuffle(t) for t in temp]

	features = [t[:,0:numPredictors] for t in temp]
	targets = [t[:,-1] for t in temp]

	#--------------- Remove the features and targets from the test domain --------------

	featuresTest = [features[-1]]
	targetTest = [targets[-1]]

	del features[-1]
	del targets[-1]

	numPredictors = features[0].shape[1]

	#--------------- Pool together the features and targets from the training domains ---------------

	X = features[0]
	X = np.append(X,features[1],axis=0)
	X = np.append(X,features[2],axis=0)

	Y = targets[0]
	Y = np.append(Y,targets[1])
	Y = np.append(Y,targets[2])
	Y = Y[:,np.newaxis]

	ls = 0.5
	lam = 0.999
	isPoly = 0

	numSamplesOpti = 150
	SaveOpti = 'optimProvi'+ str(indexT)

	beta = np.random.normal(0,0.001,numPredictors)
	beta = np.zeros(numPredictors)

    #--------------- If 1, optimize (9) and save the resulting beta. Otherwise read from file ---------------

	if not(save):
		with open(SaveOpti,'rb') as f:
			betaO = pickle.load(f)
	else:
		featOpti = [f[0:numSamplesOpti,:] for f in features]
		targOpti = [t[0:numSamplesOpti] for t in targets]

		# Parameters for optimisation
		par = params(featOpti, lam, isPoly, ls)
		betaO = optimize.fmin_cg(lossFunctionPairwise, beta,args=(featOpti,targOpti,par),fprime=gradientPairwise,maxiter=iterations)
		with open(SaveOpti,'wb') as f:
			pickle.dump(betaO,f)

		#betaO1va = optimize.fmin_cg(lossFunctionPool, beta,args=(featOpti,targOpti,par),fprime=gradientPool,maxiter=iterations)
		#SaveOpti1va = SaveOpti + '1va'
		#with open(SaveOpti1va,'wb') as f:
		#	pickle.dump(betaO,f)

	SaveIndex = 'indexOpti' + str(indexT)

	# --------------- If 1, find the invariant set S going through increasing subsets. Otherwise read from file ---------------

	folds = 10
	par = params(features, 0.9999, isPoly, ls, folds)
	# indexC = getInvariantSet(betaO,paramGrid,X,Y.flatten(),features,targets,par)
	# print indexC.shape
	# par = params(features, 0.9995, isPoly, ls, folds)
	# indexC = getInvariantSet(betaO,paramGrid,X,Y.flatten(),features,targets,par)
	# print indexC.shape
	# par = params(features, 0.9999, isPoly, ls, folds)
	# indexC = getInvariantSet(betaO,paramGrid,X,Y.flatten(),features,targets,par)
	# print indexC.shape
	# par = params(features, 0.99995, isPoly, ls, folds)
	# indexC = getInvariantSet(betaO,paramGrid,X,Y.flatten(),features,targets,par)
	# print indexC.shape
	# sys.exit()
	# par = params(features, 1, isPoly, ls, folds)
	# indexC = getInvariantSet(betaO,paramGrid,X,Y.flatten(),features,targets,par)
	# print indexC.shape
	# sys.exit()

	if save:

		indexC = getInvariantSet(betaO,paramGrid,X,Y.flatten(),features,targets,par)

		with open(SaveIndex,'wb') as f:
			pickle.dump(indexC,f)

		#indexC1va = getInvariantSet(betaO1va,paramGrid,X,Y.flatten(),features,targets,par)
		#SaveIndex1va = SaveIndex + '1va'

		#with open(SaveIndex1va,'wb') as f:
			#pickle.dump(indexC1va,f)
	else:

		with open(SaveIndex,'rb') as f:
			indexC = pickle.load(f)

	indexC = np.sort(indexC)
	print indexC
	print indexC.shape

	#--------------- Get the complementary set N = {1...p} \ S ---------------

	mask = np.ones(numPredictors,dtype=bool)
	mask[indexC] = False
	index_nC = np.arange(numPredictors)[mask]

	#MATRICES FOR MMD TEST -------------
	if computeMMD:

		stringArray = ['matTrainResiduals','matTestResiduals','matTrainResidualsInv','matTestResidualsInv']
		getMatricesForMMD(X,Y.flatten(),featuresTest[0],targetTest[0],indexC,paramGrid,stringArray,par,False,True)

	mask = np.ones(targetTest[0].size,dtype = bool )
	mask[indexSamplesTest] = False
	featureTest_test = [featuresTest[0][1000:,:]]  # [featuresTest[0][mask,:]]
	targetTest_test = [targetTest[0][1000:]]#[targetTest[0][mask]]

	# best_regInv, best_score = crossVal(paramGrid,X[:,indexC],Y,False,10)#0.1
	# best_regInv = 0
	# regr = linear_model.Ridge(alpha = best_regInv)
	# regr.fit(X[:,indexC],Y)
	#
	# alphaC = regr.coef_[0]
	# ResTrain = (Y.flatten()-regr.predict(X[:,indexC]).flatten())[:,np.newaxis]
	# ResTest = ( targetTest[0].flatten()-regr.predict(featuresTest[0][:,indexC]).flatten())

	plotStuff = 0
	ind = 0
	errors = np.zeros((6,samplesTestArr.size))
	errors[0,:] = samplesTestArr

	for nsT in samplesTestArr:

		st = int(0.5*nsT)
       		indexSamplesTest = np.arange(2*st)

		featureTest_train = [featuresTest[0][indexSamplesTest,:]]
		targetTest_train = [targetTest[0][indexSamplesTest][:,np.newaxis]]

		targetTestIter = targetTest_test[0].flatten()

		Xiter = np.append(X,featureTest_train[0], axis=0)
		Yiter = np.append(Y,targetTest_train[0], axis = 0)

		# ESTIMATE ALPHAC AND EPSILON USING THE POOLED DATA

		best_regInv, best_score = crossVal(paramGrid,Xiter[:,indexC],Yiter,False,10)

		regr = linear_model.Ridge(alpha = best_regInv)
		regr.fit(Xiter[:,indexC],Yiter)
		print ( "Shape of alphaC %f" , regr.coef_.shape)
		alphaC = regr.coef_[0]
		epsEst = np.mean((Yiter-regr.predict(Xiter[:,indexC]))**2)
		predC = regr.predict(featureTest_test[0][:,indexC]).flatten()

		# ESTIMATE DOMAIN INDEPENDENT BETA

		best_reg, best_score = crossVal(paramGrid,featureTest_train[0],targetTest_train[0],False,5)
		regr = linear_model.Ridge(alpha = best_reg)

		regr.fit(featureTest_train[0], targetTest_train[0])
		betaDomOp = regr.coef_
		predOpt = regr.predict(featureTest_test[0]).flatten()


		index_nC = np.arange(numPredictors)
		mask = np.ones(numPredictors, dtype=bool)
		mask[indexC] = False
		index_nC = index_nC[mask]
		feature_C = featureTest_train[0][:,indexC]
		feature_nC = featureTest_train[0][:,index_nC]


		# ESTIMATE BETA INV+

		best_reg, b = crossVal_infp(paramGrid,epsEst,np.array(feature_C),np.array(feature_nC),targetTest_train[0].flatten(),alphaC,nsT,5)
		betaC, betaE = optimalCoefs(alphaC,epsEst,np.array(feature_C),np.array(feature_nC),targetTest_train[0].flatten(),nsT,len(indexC),len(index_nC),best_reg)
		predCool = np.sum(betaC*featureTest_test[0][:,indexC],1) + np.sum(betaE*featureTest_test[0][:,index_nC],1)
		predCool = predCool.flatten()

		# ESTIMATE THE POOLED APPROACHES (RIDGE AND LASSO)

		best_reg,  best_score = crossVal(paramGrid,Xiter,Yiter,False,10)
		regr = linear_model.Ridge(alpha = best_reg)
		regr.fit(Xiter,Yiter)

		betaPool = regr.coef_[0]
		predPool =  regr.predict(featureTest_test[0]).flatten()

		paramsLasso = [1e-10,0.0001,0.001,0.01,0.1,1,10]
		best_reg,  best_score = crossVal(paramsLasso,Xiter,Yiter,True,10)
		regr = linear_model.Lasso(alpha = best_reg)
		regr.fit(Xiter,Yiter)
		predPoolLasso = regr.predict(featureTest_test[0]).flatten()


		# PRINT EXPLAINED VARIANCE AND PERCENTAGE OF CORRECTLY CLASSIFIED AS POSITIVE OR NEGATIVE

		print("Accuracy pooled prediction Ridge: %f, %f" % (explainedVariance(predPool, targetTestIter),accuracy(predPool,targetTestIter)))
		print("Accuracy pooled prediction Lasso: %f, %f" % (explainedVariance(predPoolLasso, targetTestIter),accuracy(predPoolLasso,targetTestIter)))
		print("Accuracy dom on prediction: %f, %f" % (explainedVariance(predOpt,targetTestIter),accuracy(predOpt,targetTestIter)))
		print("Accuracy cool prediction: %f, %f" % (explainedVariance(predCool, targetTestIter),accuracy(predCool,targetTestIter)))
		print("Accuracy cau prediction: %f,%f" % (explainedVariance(predC,targetTestIter),accuracy(predC,targetTestIter)))

		# SAVING THE EXPLAINED VARIANCES

		errors[1,ind] = explainedVariance(predOpt,targetTestIter)
		errors[2,ind] = explainedVariance(predCool,targetTestIter)
		errors[3,ind] = explainedVariance(predPool,targetTestIter)
		errors[4,ind] = explainedVariance(predC,targetTestIter)
		errors[5,ind] = explainedVariance(predPoolLasso,targetTestIter)


		ind = ind+1

   	# ---------- SAVE ERRORS INTO FILE FOR SUBSEQUENT PLOTS ------------
	errors_fileIter = errors_file + str(indexT)
	with open(errors_fileIter,'wb') as f:
		pickle.dump(errors,f)
